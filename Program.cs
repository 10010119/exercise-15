﻿using System;

namespace exercise_15
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1 = 1;
            var num2 = 2;
            var num3 = 3;
            var num4 = 4;
            var num5 = 5;
            var num6 = 6;
            var num7 = 7;
            var num8 = 8;
            var num9 = 9;
            var num10 = 10;
            var num11 = 11;
            var num12 = 12;

            Console.WriteLine($"The value of num1 = {num1}");
            Console.WriteLine($"The value of num2 = {num2}");
            Console.WriteLine($"The value of num3 = {num3}");
            Console.WriteLine($"The value of num4 = {num4}");
            Console.WriteLine($"The value of num5 = {num5}");
            Console.WriteLine($"The value of num6 = {num6}");
            Console.WriteLine($"The value of num7 = {num7}");
            Console.WriteLine($"The value of num8 = {num8}");
            Console.WriteLine($"The value of num9 = {num9}");
            Console.WriteLine($"The value of num10 = {num10}");
            Console.WriteLine($"The value of num11 = {num11}");
            Console.WriteLine($"The value of num12 = {num12}");

            Console.Clear();

            Console.WriteLine($"{num1} * {num1} = {num1}");
            Console.WriteLine($"{num1} * {num2} = {num2}");
            Console.WriteLine($"{num1} * {num3} = {num3}");
            Console.WriteLine($"{num1} * {num4} = {num4}");
            Console.WriteLine($"{num1} * {num5} = {num5}");
            Console.WriteLine($"{num1} * {num6} = {num6}");
            Console.WriteLine($"{num1} * {num7} = {num7}");
            Console.WriteLine($"{num1} * {num8} = {num8}");
            Console.WriteLine($"{num1} * {num9} = {num9}");
            Console.WriteLine($"{num1} * {num10} = {num10}");
            Console.WriteLine($"{num1} * {num11} = {num11}");
            Console.WriteLine($"{num1} * {num12} = {num12}");

            Console.WriteLine("Hello User, Please type in a number to multiply by 12");

            var num34 = int.Parse(Console.ReadLine());

            Console.WriteLine($"The number you typed in multiplied by 12 is {num34 * 12}");
        }
    }
}
